#ifndef LLMO_EXTSTR_VMT_T_H
#define LLMO_EXTSTR_VMT_T_H

#include <cstddef>
#include <cstdint>

namespace llmo::extstr {
	class vmt_t {
		struct object_t {
			struct vtbl_t {
				std::uintptr_t fn[2];
			};

			vtbl_t *vtbl;
		};

		std::uint8_t	 *page_   = nullptr;
		std::size_t	   used_   = 0;
		std::uintptr_t caller_ = 0;

	public:
		/**
		 * @brief Construct class to control extend structs
		 * @param pageCount Count of memory pages for builtin code allocation
		 */
		vmt_t( std::size_t pageCount = 1 );
		~vmt_t() = delete;

		typedef void ( *extdtor_t )( void *extptr, void *ctx );

		/**
		 * @brief Generate dtor hook to access to extend struct data
		 * @param object Pointer to object with virtual table
		 * @param data Extend struct data
		 * @param deleter Callback to remove extend struct data
		 * @param mem Memory to create code. When passed `nullptr` will be use llmo::mem allocator
		 * @return Pointer to hook
		 */
		void *generateExtend( void *object, void *data, const extdtor_t &deleter, void *context, void *mem = nullptr );

		/**
		 * @brief Reuse hook for multiple objects
		 * @param object Pointer to object with virtual table
		 * @param hook Pointer to hook
		 */
		static void reuseHook( void *object, void *hook );

		/**
		 * @brief Swap pointer to extend struct data
		 * @details Must have same deleter
		 * @param hook Pointer to hook
		 * @param data New extend struct data
		 * @return Old extend struct data
		 */
		static void *swapExtend( void *hook, void *data );

		/**
		 * @brief Swap pointer to deleter of extend struct data
		 * @param hook Pointer to hook
		 * @param deleter New deleter
		 * @return Pointer to old deleter
		 */
		static void *swapDeleter( void *hook, const extdtor_t &deleter, void *context );
		
		/// @return Size of hook code for current platform
		static std::size_t getHookCodeSize();

		/**
		 * @brief Get extend struct data from object
		 * @param object Object with extend struct data
		 * @return Pointer to Extend struct data or nullptr if object does not contain extend struct data (doesn't work with MSVC and 3rd-party dtor hooks)
		 */
		void *getExtendData( void *object );

		/**
		 * @brief Check is object own extend data
		 * @param object Object to check
		 * @return @b true if object have extend data
		 */
		bool hasExtendData(void *object);

	protected:
		void generateEnterCode();
	};
} // namespace llmo::extstr

#endif // LLMO_EXTSTR_VMT_T_H

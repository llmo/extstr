#include "vmt.h"

#include <cstring>

#include <stdexcept>

#include "inc_depends.h"

namespace details {
	static constexpr std::uint8_t kDtorX86[] = {
		0x58,						  // pop eax
		0x3D, 0x00, 0x00, 0x00, 0x00, // cmp eax, 0x00000000 (caller)
		0x50,						  // push eax
		0xB8, 0x00, 0x00, 0x00, 0x00, // mov eax, 0x00000000 (stored ptr)
		0x75, 0x01,					  // jne ORIG_DTOR
		0xC3,						  // ret
									  // ORIG_DTOR
		0x51,						  // push ecx
		0x68, 0x00, 0x00, 0x00, 0x00, // push 0x00000000 (context)
		0x50,						  // push eax
		0xB8, 0x00, 0x00, 0x00, 0x00, // mov eax, 0x00000000 (deleter)
		0xFF, 0xD0,					  // call eax (deleter)
		0x83, 0xC4, 0x08,			  // add esp, 8
		0x59,						  // pop ecx
		0xE9, 0x00, 0x00, 0x00, 0x00, // jmp dtor
		0xCC, 0xCC                    // int3; int3
	};
	static constexpr std::uint8_t kDtorX86_64[] = {
		0x48, 0xB8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov eax, 0x0000000000000000 (caller)
		0x48, 0x3B, 0x04, 0x24,										// cmp rax, [rsp]
		0x48, 0xB8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov rax, 0x0000000000000000 (stored ptr)
		0x75, 0x01,													// jne ORIG_DTOR
		0xC3,														// ret
																	// ORIG_DTOR
#ifdef PLATFORM_ABI_WINDOWS
		0x51,														// push rcx
		0x52,														// push rdx
		0x48, 0x89, 0xC1,											// mov rcx, rax
		0x48, 0xBA, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov rdx, 0x0000000000000000 (context)
#else
		0x57,														// push rdi
		0x56,														// push rsi
		0x48, 0x89, 0xC7,											// mov rdi, rax
		0x48, 0xBE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov rsi, 0x0000000000000000 (context)
#endif
		0x48, 0xB8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov rax, 0x0000000000000000 (deleter)
		0xFF, 0xD0,													// call rax (deleter)
#ifdef PLATFORM_ABI_WINDOWS
		0x5A, // pop rdx
		0x59, // pop rcx
#else
		0x5E,														// pop rsi
		0x5F,														// pop rdi
#endif
		0x48, 0xB8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov rax, 0x0000000000000000 (dtor)
		0xFF, 0xE0,													// jmp rax (dtor)
	};
	static constexpr std::uint8_t kDtorArm32[] = {
		0x01, 0xB4,										// push {r0}
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov r0, 0x00000000 (caller)
		0x70, 0x45,										// cmp r0, lr
		0x01, 0xBC,										// pop {r0}
		0xFC, 0xD1,										// bne ORIG_DTOR
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov r0, 0x00000000 (stored ptr)
		0x70, 0x47,										// bx lr
		0x00, 0xB5,										// push {lr}
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // mov lr, 0x00000000 (ORIG_DTOR)
		0x70, 0x47,										// bx lr
		0x00, 0xBD,										// pop {pc}

		// TODO: deleter
	};

	static constexpr std::uint8_t kEnterX86[] = {
	// Also it for x86_64
#ifdef PLATFORM_ABI_WINDOWS
		0xFF, 0xD1, // call (e|r)cx
#else
		0xFF, 0xD7,													// call (e|r)di
#endif
		0xC3, // ret
	};
	static constexpr std::uint8_t kEnterArm32[] = {
		0x00, 0xB5, // push {lr}
		0x00, 0x47, // bx r0
		0x00, 0xBD	// pop {pc}
	};
} // namespace details

llmo::extstr::vmt_t::vmt_t( std::size_t pageCount ) {
#ifndef WITHOUT_MEM_DEP
	page_ = static_cast<std::uint8_t *>( mem::allocate( pageCount ) );
	mem::prot::set( page_, mem::pageSize() );
#	ifdef PLATFORM_ARCH
	if constexpr ( PLATFORM_ARCH == platforms::arch::x86 || PLATFORM_ARCH == platforms::arch::x86_64 )
		std::memset( page_, 0xCC, mem::pageSize() ); // int 3 (BP)
#	endif
	generateEnterCode();
#endif
}

void *llmo::extstr::vmt_t::generateExtend( void *object, void *data, const extdtor_t &deleter, void *context,
										   void *mem ) {
	const std::uint8_t *code = nullptr;
	std::size_t			size = 0;
#ifdef PLATFORM_ARCH
	if constexpr ( PLATFORM_ARCH == platforms::arch::x86_64 ) {
		code = details::kDtorX86_64;
		size = sizeof( details::kDtorX86_64 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::x86 ) {
		code = details::kDtorX86;
		size = sizeof( details::kDtorX86 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::armv7a ) {
		code = details::kDtorArm32;
		size = sizeof( details::kDtorArm32 );
	}
#endif
	if ( !code || !size ) return nullptr;

	if ( !mem ) {
		if ( mem::pageSize() - ( used_ + 1 ) > size ) {
			mem = (void *)&page_[used_ + 1];
			used_ += size + 1;
		} else {
#ifndef WITHOUT_MEM_DEP
			mem = mem::allocate();
#else
			return nullptr;
#endif
		}
	}

	std::memcpy( mem, code, size );
#ifdef PLATFORM_ARCH_armv7a
	try {
#endif
		swapExtend( mem, data );
		swapDeleter( mem, deleter, context );
#ifdef PLATFORM_ARCH_armv7a
	} catch ( ... ) {
	}
#endif

#ifndef WITHOUT_ASSEMBLER_DEP
#	if defined( PLATFORM_ABI_WINDOWS ) /*&& !defined( __GNUC__ )*/
	auto dtor = ( (object_t *)object )->vtbl->fn[0];
#	else
	auto dtor = ( (object_t *)object )->vtbl->fn[1];
#	endif
#	ifdef PLATFORM_ARCH_x86_64
	assembler::branch::makeDirectJmp( (void *)( std::uintptr_t( mem ) + 0x38 ), (void *)dtor,
									  platforms::cpu::registers::RAX );
#	elif defined( PLATFORM_ARCH_x86 )
	assembler::branch::makeRelativeJmp( (void *)( std::uintptr_t( mem ) + 0x21 ), (void *)dtor );
#	elif defined( PLATFORM_ARCH_armv7a )
	assembler::branch::makeDirectCall( (void *)( std::uintptr_t( mem ) + 0x1C ), (void *)dtor,
									   platforms::cpu::registers::lr );
#	endif
#else
	return nullptr;
#endif

	auto callerAddr = std::uintptr_t( mem ) + 2;
#ifdef PLATFORM_ARCH_x86
	*(std::uintptr_t *)callerAddr = caller_;
#else
	assembler::detail::MOV( callerAddr, caller_, platforms::cpu::registers::r0 );
#endif

	reuseHook( object, mem );

	return mem;
}

void llmo::extstr::vmt_t::reuseHook( void *object, void *hook ) {
	auto prot = mem::prot::get( &( (object_t *)object )->vtbl->fn, sizeof( void * ) );
	mem::prot::set( &( (object_t *)object )->vtbl->fn, sizeof( void * ) );
#if defined( PLATFORM_ABI_WINDOWS ) /*&& !defined( __GNUC__ )*/
	( (object_t *)object )->vtbl->fn[0] = (std::uintptr_t)hook;
#else
	( (object_t *)object )->vtbl->fn[1] = (std::uintptr_t)hook;
#endif
	mem::prot::set( &( (object_t *)object )->vtbl->fn, sizeof( void * ), prot );
}

void *llmo::extstr::vmt_t::swapExtend( void *hook, void *data ) {
#ifdef PLATFORM_ARCH
	void **pData = nullptr;
	if constexpr ( PLATFORM_ARCH == platforms::arch::x86_64 ) {
		pData = (void **)( std::uintptr_t( hook ) + 0x10 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::x86 ) {
		pData = (void **)( std::uintptr_t( hook ) + 0x08 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::armv7a ) {
		// TODO: read old data
#	ifdef PLATFORM_ARCH_armv7a
		assembler::detail::MOV( std::uintptr_t( hook ) + 0x10, (int)data, platforms::cpu::registers::r0 );
#	endif
		throw std::runtime_error( __func__ + std::string( " not implimented for armv7a" ) );
	}
	if ( pData ) {
		auto oldData = *pData;
		*pData		 = data;
		return oldData;
	}
#endif
	return nullptr;
}

void *llmo::extstr::vmt_t::swapDeleter( void *hook, const extdtor_t &deleter, void *context ) {
#ifdef PLATFORM_ARCH
	void **pDeleter = nullptr;
	void **pContext = nullptr;
	if constexpr ( PLATFORM_ARCH == platforms::arch::x86_64 ) {
		pDeleter = (void **)( std::uintptr_t( hook ) + 0x2C );
		pContext = (void **)( std::uintptr_t( hook ) + 0x22 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::x86 ) {
		pDeleter = (void **)( std::uintptr_t( hook ) + 0x17 );
		pContext = (void **)( std::uintptr_t( hook ) + 0x11 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::armv7a ) {
		throw std::runtime_error( __func__ + std::string( " not implimented for armv7a" ) );
	}
	if ( pDeleter ) {
		auto oldDeleter = *pDeleter;
		*pDeleter		= (void *)deleter;
		*pContext		= context;
		return oldDeleter;
	}
#endif
	return nullptr;
}

std::size_t llmo::extstr::vmt_t::getHookCodeSize(){
	std::size_t size = 0;
#ifdef PLATFORM_ARCH
	if constexpr ( PLATFORM_ARCH == platforms::arch::x86_64 ) {
		size = sizeof( details::kDtorX86_64 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::x86 ) {
		size = sizeof( details::kDtorX86 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::armv7a ) {
		size = sizeof( details::kDtorArm32 );
	}
#endif
	if ( size == 0 ) return size;
	
	auto align = size % sizeof(void*);
	if (align != 0) size += sizeof(void*) - align;
	
	return size;
}

void *llmo::extstr::vmt_t::getExtendData( void *object ) {
#if !defined( WITHOUT_MEM_DEP ) && ( !defined( PLATFORM_ABI_WINDOWS ) /*|| defined( __GNUC__ )*/ )
	auto pageD0 = ( (object_t *)object )->vtbl->fn[0] & mem::makeMask( mem::pageSize() );
	auto pageD2 = ( (object_t *)object )->vtbl->fn[1] & mem::makeMask( mem::pageSize() );
	if ( pageD0 == pageD2 || pageD2 == pageD0 + mem::pageSize() ) return nullptr;
#endif
#if defined( PLATFORM_ABI_WINDOWS ) /*&& !defined( __GNUC__ )*/
	auto dtor = ( (object_t *)object )->vtbl->fn[0];
#else
	auto dtor = ( (object_t *)object )->vtbl->fn[1];
#endif
#if defined( PLATFORM_ARCH_x86 ) && !defined( PLATFORM_ARCH_x86_64 )
#	ifdef PLATFORM_ABI_WINDOWS
	return ( ( void *(__fastcall *)( std::uintptr_t ) )( page_ ) )( dtor );
#	else
	typedef void *( *enter_t )( std::uintptr_t ) __attribute__( ( fastcall ) );
	return ( enter_t( page_ ) )( dtor );
#	endif
#else
	return ( ( void *(*)( std::uintptr_t ) )( page_ ) )( dtor );
#endif
}

bool llmo::extstr::vmt_t::hasExtendData( void *object ) {
	// Compare pages of code dtors. This leave on MSVC ABI to check with first method
	auto pageD0 = ( (object_t *)object )->vtbl->fn[0] & mem::makeMask( mem::pageSize() );
	auto pageD2 = ( (object_t *)object )->vtbl->fn[1] & mem::makeMask( mem::pageSize() );
	if ( pageD0 == pageD2 || pageD2 == pageD0 + mem::pageSize() ) return false;

	const std::uint8_t *code = nullptr;
#ifdef PLATFORM_ARCH
	if constexpr ( PLATFORM_ARCH == platforms::arch::x86_64 ) {
		code = details::kDtorX86_64;
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::x86 ) {
		code = details::kDtorX86;
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::armv7a ) {
		code = details::kDtorArm32;
	}
#endif
	if ( code == nullptr ) return false;

	auto dtor = reinterpret_cast<const std::uint8_t *>( ( (object_t *)object )->vtbl->fn[0] );
	std::size_t diff_bytes = 0;
	for ( int i = 0; i < getHookCodeSize(); ++i ) {
		if ( dtor[i] != code[i] ) ++diff_bytes;
	}

	if constexpr ( PLATFORM_ARCH == platforms::arch::x86_64 ) {
		return diff_bytes <= 40;
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::x86 ) {
		return diff_bytes <= 20;
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::armv7a ) {
		return diff_bytes <= 24;
	}

	return false;
}

void llmo::extstr::vmt_t::generateEnterCode() {
#if !defined( WITHOUT_MEM_DEP ) && defined( PLATFORM_ARCH )
	if constexpr ( PLATFORM_ARCH == platforms::arch::x86 || PLATFORM_ARCH == platforms::arch::x86_64 ) {
		std::memcpy( page_, details::kEnterX86, sizeof( details::kEnterX86 ) );
		used_ += sizeof( details::kEnterX86 );
	} else if constexpr ( PLATFORM_ARCH == platforms::arch::armv7a ) {
		std::memcpy( page_, details::kEnterArm32, sizeof( details::kEnterArm32 ) );
		used_ += sizeof( details::kEnterArm32 );
	}
	if ( used_ ) caller_ = std::uintptr_t( page_ ) + ( used_ - 1 );
#endif
}

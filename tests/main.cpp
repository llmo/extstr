#include <iostream>
#include "../vmt.h"

class Test {
public:
	virtual ~Test() { std::cout << "Test is deleted" << std::endl; }
	virtual void Foo() { std::cout << "Foo" << std::endl; }
};

class DTest : public Test {
public:
	virtual ~DTest() { std::cout << "DTest is deleted" << std::endl; }
	void Foo() override { std::cout << "Bar" << std::endl; }
};

static const auto kFakeExtData = (void *)1337;

void fakeDeleter( void *data, void */*ctx*/ ) {
	std::cout << "Delete fake data " << data << std::endl;
}

int main() {
	Test *test = new DTest();

	auto extstr = new llmo::extstr::vmt_t();

	auto hook = extstr->generateExtend( test, kFakeExtData, fakeDeleter, nullptr );
	std::cout << "Hook at " << std::hex << hook << std::endl;

	test->Foo();

	auto data = extstr->getExtendData( test );
	std::cout << "Fake data: " << data << std::endl;
	if ( data != kFakeExtData ) return 1;

	delete test;
	return 0;
}

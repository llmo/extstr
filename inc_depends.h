#ifndef INC_DEPENDS_H
#define INC_DEPENDS_H

#if __has_include( "assembler/asm.h" )
#	include "assembler/asm.h"
#elif __has_include( <assembler/asm.h>)
#	include <assembler/asm.h>
#elif __has_include( "asm.h" )
#	include "asm.h"
#elif __has_include( <asm.h>)
#	include <asm.h>
#else
#	define WITHOUT_ASSEMBLER_DEP
#endif

#if defined( PLATFORM_ARCH_armv7a ) && !defined( WITHOUT_ASSEMBLER_DEP )
#	if __has_include( "assembler/asm/armv7a.h" )
#		include "assembler/asm/armv7a.h"
#	elif __has_include( <assembler/asm/armv7a.h>)
#		include <assembler/asm/armv7a.h>
#	elif __has_include( "asm/armv7a.h" )
#		include "asm/armv7a.h"
#	elif __has_include( <asm/armv7a.h>)
#		include <asm/armv7a.h>
#	endif
#endif

#if __has_include( "mem/mem.h" )
#	include "mem/mem.h"
#elif __has_include( <mem/mem.h>)
#	include <mem/mem.h>
#elif __has_include( "mem.h" )
#	include "mem.h"
#elif __has_include( <mem.h>)
#	include <mem.h>
#else
#	define WITHOUT_MEM_DEP
#endif

#endif // INC_DEPENDS_H
